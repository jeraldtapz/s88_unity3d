﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
    #region Private Fields
    [SerializeField]
    private Transform target;

    [SerializeField]
    private Vector3 offset;

    [SerializeField][Range(0f,1f)]
    private float smoothingFactor;
    #endregion

    #region Unity Life Cycle

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position + offset, smoothingFactor);
    }
    #endregion

}
