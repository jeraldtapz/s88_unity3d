﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    #region Property Fields
    [SerializeField]
    private float WalkSpeed;

    [SerializeField]
    private float RunSpeed;

    [SerializeField]
    private float TurnRateSmoothingFactor;          

    [SerializeField]
    private float TranslateSmoothingFactor;

    private Animator animator;
    private Vector2 input;
    private Vector2 inputDir;
    private bool isRunning;

    
    private float targetSpeed;                          //Used for smoothing
    private float currentSpeed;                          //Used for smoothing
    private Vector3 targetEulerAngles;                  //Used for smoothing


    private float turnRateSmoothVelocity;               //Used for smoothing the turning 
    private float translateSmoothVelocity;              //Used for smoothing movement

    private bool isInJumpMotion;
    #endregion

    #region Unity Life Cycle
    private void Start()
    {
        input = new Vector2();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        GetInput();
        HandleRotation();
        HandleTranslation();
        HandleJump();
    }

    #endregion

    #region Private Functionality

    private void HandleJump()
    {
        if (!isInJumpMotion &&  Input.GetKeyDown(KeyCode.Space))
        {
            animator.applyRootMotion = true;
            isInJumpMotion = true;
            animator.SetTrigger("Jump");
        }
    }

    private void HandleTranslation()
    {
        if (isInJumpMotion)
            return;
        isRunning = Input.GetKey(KeyCode.LeftShift);
        targetSpeed = (isRunning ? RunSpeed : WalkSpeed) * inputDir.magnitude;
        currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref translateSmoothVelocity, TranslateSmoothingFactor);

        transform.Translate(transform.forward * currentSpeed * Time.deltaTime, Space.World);

        float animationSpeed = (isRunning ? 1 : .5f) * inputDir.magnitude;
        animator.SetFloat("Speed", animationSpeed, TranslateSmoothingFactor, Time.deltaTime);
    }

    private void HandleRotation()
    {
        if (isInJumpMotion)
            return;
        if (inputDir != Vector2.zero)
        {
            targetEulerAngles = Vector3.up * Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg;
            transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetEulerAngles.y, ref turnRateSmoothVelocity, TurnRateSmoothingFactor);
        }
    }

    private void GetInput()
    {
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");

        inputDir = input.normalized;
    }

    #endregion

    #region Public Functionality

    //This is used as an Animation Event on JumpFWD animation
    //JumpFWD is just a duplicate of one of the provided animations
    public void DisableRootMotion()
    {
        animator.applyRootMotion = false;
        isInJumpMotion = false;
    }
    
    #endregion

}
