﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CanvasData
{
    public class UICanvas : MonoBehaviour
    {
        #region Property Fields
        [SerializeField]
        private int CurrentIndex = 0;

        [SerializeField]
        private UIData CurrentUIData;

        [SerializeField]
        private List<UIData> UIDataList;

        [SerializeField]
        private GameObject[] TemplatePrefabs;

        [SerializeField]
        private Transform PanelContentTransform;

        [SerializeField]
        private int MaxTemplateColorPerRow;

        private List<GameObject> TemplateItemInstances;
        private List<GameObject> TemplateRangeInstances;
        private List<GameObject> TemplatePanelColorInstances;
        private List<GameObject> TemplatePanelColorParentInstances;

        #endregion

        #region Unity Life Cycle
        private void Awake()
        {
            TemplateItemInstances = new List<GameObject>();
            TemplateRangeInstances = new List<GameObject>();
            TemplatePanelColorInstances = new List<GameObject>();
            TemplatePanelColorParentInstances = new List<GameObject>();
            LoadData(0);
        }
        #endregion

        #region Private Functionality
        private void LoadData(int index)
        {
            CurrentIndex = Mathf.Clamp(index, 0, UIDataList.Count - 1);
            CurrentUIData = UIDataList[CurrentIndex];
            GenerateUI();
        }

        /// <summary>
        /// This deactivates all instances of the UI templates
        /// </summary>
        private void DeactivateUITemplateInstances()
        {
            for (int i = 0; i < TemplateItemInstances.Count; i++)
            {
                TemplateItemInstances[i].SetActive(false);
            }

            for (int i = 0; i < TemplateRangeInstances.Count; i++)
            {
                TemplateRangeInstances[i].SetActive(false);
            }

            for (int i = 0; i < TemplatePanelColorInstances.Count; i++)
            {
                TemplatePanelColorInstances[i].SetActive(false);
            }
            for (int i = 0; i < TemplatePanelColorParentInstances.Count; i++)
            {
                TemplatePanelColorParentInstances[i].SetActive(false);
            }
            //if(TemplatePanelColorParent)
            //    TemplatePanelColorParent.SetActive(false);
        }


        /// <summary>
        /// Generates the Items from UIData
        /// </summary>
        private void GenerateTemplateItems()
        {
            int itemCount = CurrentUIData.Items.Count;
            int counter = 0;

            //Search the list of instances for possible item reuse
            for (counter = 0; counter < TemplateItemInstances.Count; counter++)
            {
                //reuse instances up to itemCount instances
                if (counter < itemCount)
                {
                    Transform objTransform = TemplateItemInstances[counter].transform;
                    objTransform.gameObject.SetActive(true);
                    objTransform.GetChild(0).GetComponent<Text>().text = CurrentUIData.Items[counter];
                }
            }

            //this happens when the number of currently available instances is less than how much we need to generate
            //so we instantiate new instances
            if (counter < itemCount)
            {
                for (; counter < itemCount; counter++)
                {
                    Transform objTransform = Instantiate(TemplatePrefabs[0], PanelContentTransform).transform;
                    objTransform.GetChild(0).GetComponent<Text>().text = CurrentUIData.Items[counter];

                    TemplateItemInstances.Add(objTransform.gameObject);
                }
            }
        }


        /// <summary>
        /// Generates the RangeList from UIData
        /// </summary>
        private void GenerateTemplateRangeList()
        {
            int rangeListCount = CurrentUIData.RangeList.Count;
            int counter = 0;

            //reuse instances up to rangeListCount instances
            for (counter = 0; counter < TemplateRangeInstances.Count; counter++)
            {
                if(counter < rangeListCount)
                {
                    Transform objTransform = TemplateRangeInstances[counter].transform;
                    objTransform.gameObject.SetActive(true);
                    SetSliderMinMaxValue(counter, objTransform);
                }
            }

            //if the amount of currently reusable instances are not enough
            //we instantiate new instances
            if(counter < rangeListCount)
            {
                for (; counter < rangeListCount; counter++)
                {
                    Transform objTransform = Instantiate(TemplatePrefabs[1], PanelContentTransform).transform;
                    SetSliderMinMaxValue(counter, objTransform);

                    TemplateRangeInstances.Add(objTransform.gameObject);
                }
            }
        }
        private void SetSliderMinMaxValue(int counter, Transform objTransform)
        {
            //set the data
            UIData.RangeData rangeData = CurrentUIData.RangeList[counter];
            objTransform.GetComponent<UIRangeHelper>().SetUpRangeValues(rangeData);
        }


        /// <summary>
        ///Generates the ColorUI from UIData 
        /// </summary>
        private void GenerateTemplateColors()
        {
            int colorPanelCount = CurrentUIData.Colors.Count;

            if(colorPanelCount > 0)
            {
                GenerateTemplateColorsForMultipleRows(colorPanelCount);
            }
        }

        private void GenerateTemplateColorsForMultipleRows(int colorPanelCount)
        {
            GenerateRows(colorPanelCount);

            for (int i = 0; i < colorPanelCount; i++)
            {
                TemplatePanelColorInstances[i].SetActive(true);
                SetPanelColorData(i, TemplatePanelColorInstances[i].transform);
            }
        }

        private void GenerateRows(int colorPanelCount)
        {
            int rowsToGenerate = colorPanelCount / MaxTemplateColorPerRow;
            if (colorPanelCount % MaxTemplateColorPerRow > 0)
                rowsToGenerate++;

            int rowCounter = 0;
            for (rowCounter = 0; rowCounter < TemplatePanelColorParentInstances.Count; rowCounter++)
            {
                if (rowCounter >= rowsToGenerate)
                    break;
                TemplatePanelColorParentInstances[rowCounter].SetActive(true);
            }


            //For each row generated, we generate 5 templateColor instances as its child
            if (rowCounter < rowsToGenerate)
            {
                for (; rowCounter < rowsToGenerate; rowCounter++)
                {
                    Transform colorParentTransform = Instantiate(TemplatePrefabs[2], PanelContentTransform).transform;
                    for (int i = 0; i < MaxTemplateColorPerRow; i++)
                    {
                        GameObject templateColorInstance = Instantiate(TemplatePrefabs[3], colorParentTransform);
                        templateColorInstance.SetActive(false);
                        TemplatePanelColorInstances.Add(templateColorInstance);
                    }
                    TemplatePanelColorParentInstances.Add(colorParentTransform.gameObject);
                }
            }
        }

        private void SetPanelColorData(int counter, Transform objTransform)
        {
            objTransform.GetComponent<Image>().color = CurrentUIData.Colors[counter].Color;
            objTransform.GetChild(0).GetComponent<Text>().text = CurrentUIData.Colors[counter].Letter;
        }

        private void SortUIInstances()
        {
            int counter = 0;
            for (int i = 0; i < TemplateItemInstances.Count; counter++, i++)
            {
                TemplateItemInstances[i].transform.SetSiblingIndex(counter);
            }

            for (int i = 0; i < TemplateRangeInstances.Count; counter++, i++)
            {
                TemplateRangeInstances[i].transform.SetSiblingIndex(counter);
            }

            for (int i = 0; i < TemplatePanelColorParentInstances.Count; counter++, i++)
            {
                TemplatePanelColorParentInstances[i].transform.SetSiblingIndex(counter);
            }
        }

        #endregion

        #region Unity Button Events
        public void Prev()
        {
            LoadData(CurrentIndex - 1);
        }

        public void Next()
        {
            LoadData(CurrentIndex + 1);
        }
        #endregion

        // *************************************************************************************************************
        // Generate UI here based on the current data
        // *************************************************************************************************************
        private void GenerateUI()
        {
            DeactivateUITemplateInstances();

            //Generating the Items
            GenerateTemplateItems();

            //Generate the RangeList
            GenerateTemplateRangeList();

            //Generate the Colors
            GenerateTemplateColors();

            //Sorts accordingly  Items -> Range -> Colors
            SortUIInstances();
        }

        
    }
}