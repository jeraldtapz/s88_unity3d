﻿using UnityEngine;
using UnityEngine.UI;

namespace CanvasData
{
    public class UIRangeHelper : MonoBehaviour
    {
        #region Property Fields
         
        [SerializeField]
        private Text sliderValueText;

        [SerializeField]
        private Slider slider;

        #endregion

        #region Public Methods
        
        public void OnValueChange()
        {
            sliderValueText.text = ((int)slider.value).ToString();
        }

        public void SetUpRangeValues(UIData.RangeData rangeData)
        {
            slider.minValue = rangeData.Min;
            slider.maxValue = rangeData.Max;
            slider.value = rangeData.Val;
        }

        #endregion
    }
}

