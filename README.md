# README #

### Synergy88 Unity3D Programming Exam ###

* 3 scenes are available in the project folder
* DataDrivenUI.unity
* UIOptimization.unity
* CharacterController.unity


### How do I get set up? ###
* Open any of the 3 scene files in the project.
* .exe and .apk builds are located here:  ----   [Builds](https://bitbucket.org/jeraldtapz/s88_unity3d/src/6f6b6ea5039a/Deliverables/?at=master)

## Notes ##
### Data Driven UI ###
* [Download .exe Build](https://bitbucket.org/jeraldtapz/s88_unity3d/raw/0c132659f70a38c38f632b730ea4df65687688b9/Deliverables/DataDrivenUI.rar)
* UIRangeHelper.cs is attached to each Template(Range) UI element.
* There is a limit of 5 Template(Color) UI elements per row, can be edited.
* The reason for the limit is because for a big enough number, e.g. 50, the Template(Color) UI elements will be crammed in 1 row and it will break the UI.

### UI Optimization ###
* [Download .apk Build](https://bitbucket.org/jeraldtapz/s88_unity3d/raw/6f6b6ea5039a4b3f85f468a18d8e3fce3f6eb144/Deliverables/UIOptimization.apk) Smaller file size, no support for x86 devices
* [Download .apk Build with x86 support](https://bitbucket.org/jeraldtapz/s88_unity3d/raw/2a202e835957fac7a2fa2e242ff0128e9ca83e03/Deliverables/UIOptimization_with_x86support.apk) Bigger file size, supports both ARM and x86 devices
* This scene is a copy of Data Driven UI except for the sprites used and some optimizations.
* Used a sprite atlas for all the sprites used in the scene
* Disabled Allow MSAA option in the Camera
* Disabled Allow HDR option in the Camera
* Data Driven UI scene's batches ranged from 8-13. 
* After optimization, UI Optimization scene's batches ranges from 4-5 only. 

### Character Controller ###
* [Download .exe Build](https://bitbucket.org/jeraldtapz/s88_unity3d/raw/0c132659f70a38c38f632b730ea4df65687688b9/Deliverables/CharacterController.rar)
* JumpFWD animation is just a duplicate from one of the provided animations. I just don't want to edit the original animation.
* Added a simple movement and rotation smoothing
* Added a simple CameraFollow script